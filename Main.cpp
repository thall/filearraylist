#include <iostream>
#include <string>
#include <cstdlib>
#include "FileArrayList.h"

using std::cout;
using std::string;

void doTest(bool succ, string name) {
  string ret;
  if(succ){
    ret = "";
  } else {
    ret = "\033[1;31m"+name+" test failed\033[0m";
	cout << ret << "\n";
  }
}

void subTest(string message) {
  // cout << "\033[1;32m    "+message+" subtest\033[0m\n";
}

template<typename T>
bool lstIntTest(T &lst) {
  lst.clear();
  lst.push_back(1);
  lst.push_back(2);
  lst.push_back(3);
  lst.push_back(4);
  lst.push_back(5);
  lst.push_back(6);
  lst.push_back(7);
  lst.push_back(8);
  lst.push_back(9);
  if (lst[0]!=1) return false;
  if (lst[1]!=2) return false;
  if (lst[2]!=3) return false;
  if (lst[3]!=4) return false;
  if (lst[4]!=5) return false;
  if (lst[5]!=6) return false;
  if (lst[6]!=7) return false;
  if (lst[7]!=8) return false;
  if (lst[8]!=9) return false;
  subTest("pushbacks");
  lst.set(99,0);
  if (lst[0]!=99) return false;
  subTest("index assignment");
  T lst1(lst.begin(),lst.end(),"#test2#");
  T lst2(lst.begin(),lst.end(),"#test3#");
  subTest("copy");
  if(lst2.size()!=9) return false;
  subTest("size");
  lst2.pop_back();
  if(lst2.size()!=8) return false;
  subTest("pop back");
  lst2.clear();
  if(lst2.size()!=0) return false;
  subTest("clear");
  lst1.insert(++++++lst1.begin(),100);
  if(lst1[0]!=99) return false;
  if(lst1[1]!=2) return false;
  if(lst1[2]!=3) return false;
  if(lst1[3]!=100) return false;
  if(lst1[4]!=4) return false;
  if(lst1[5]!=5) return false;
  if(lst1[6]!=6) return false;
  if(lst1[7]!=7) return false;
  if(lst1[8]!=8) return false;
  if(lst1[9]!=9) return false;
  subTest("insert");
  lst.erase(lst.begin());
  if (lst[0]!=2) return false;
  subTest("remove (weak test)");
  lst1.clear();
  lst2.clear();
  return true;
}

template <typename T>
bool scalePushBack(T &lst) {
  for(int i = 0; i<2000; ++i) {
    lst.push_back(i);
    if(lst[i]!=i) return false;
  }
  subTest("pushback");
  lst.insert(lst.begin(),42); // replace with our insert args
  if(lst[0]!=42) return false;
  if(lst[2000]!=1999) return false;
  for(int i = 2000; i>0; --i) {
    if(lst[i]!=--i) return false;
  }
  subTest("large insert");
  for(int i = 2000; i>0; --i) {
    lst.pop_back();
  }
  if(lst.size()!=1) return false;
  subTest("large popback");
  return true;
}

template <typename T>
bool scaleRemove(T &lst) {
  lst.clear();
  for(int i = 0; i<2000; ++i) {
    lst.push_back(i);
    if(lst[i]!=i) return false;
  }
  subTest("pushback");
  lst.erase(lst.begin()); // replace with our insert args
  if(lst[0]!=1) return false;
  subTest("deletion");
  for(int i = 0; i<1999; ++i) {
	if(lst[i]!=i+1) return false;
  }
  lst.clear();
  return true;
}

template <typename T>
bool iterLoops(T &lst) {
  lst.clear();
  for(int i = 0; i<2000; ++i) {
    lst.push_back(i);
    if(lst[i]!=i) return false;
  }
  subTest("made iteratorloop lst");
  if(lst[*lst.begin()]!=0) return false;
  subTest("begin");
  for(auto i:lst) {
    if(lst[i]!=i) return false;
  }
  lst.clear();
  return true;
}

template <typename T>
bool persistencyTest(T &lst) {
  lst.clear();
  lst.push_back(420);
  subTest("pushback");
  T lst1(lst.begin(),lst.end(),"#leafjuice#");
  subTest("copy");
  if(lst1[0] != 420) return false;
  lst.clear();
  lst1.clear();
  return true;
}

template <typename T>
void printLst(T &lst) {
  if(lst.size()!=0) {
	for(int i=0;i<lst.size();i++) {
	  if(i!=0) cout << ", ";
	  cout << lst[i];
	}
  }
  cout << '\n';
}


int main() {
  FileArrayList<int> lst("#test#");
  doTest(lstIntTest(lst), "int list basic");
  FileArrayList<int> lstScale("#scale#");
  doTest(scaleRemove(lstScale), "scale");
  FileArrayList<int> lstIter("#iter#");
  doTest(iterLoops(lstIter), "iter");
  FileArrayList<int> lstPers("#leafjuice#");
  doTest(persistencyTest(lstPers), "file persistency");
  // Do a test to read content from the same file as another list
  return 0;
}
