#include <stdio.h>
#include <string>
template<typename T>
class FileArrayList {
  FileArrayList(const FileArrayList<T> &that) = delete;
  FileArrayList<T> operator=(const FileArrayList<T> &that) = delete;
 private:
  FILE* f;
  // store size at the beginnning of the file
  // make a function that gets size from reading it in as well
  // as a function to set size to a new value in the file
  
  static T read(int index, FILE* file) {
	fseek(file,sizeof(int)+sizeof(T)*index, SEEK_SET);
	T temp;
	fread(&temp,sizeof(T),1,file);
	return temp;
  }
  void write(const T &value, const int &index) {
	fseek(f,sizeof(int)+sizeof(T)*index, SEEK_SET);
	fwrite(&value,sizeof(T),1,f);
  }
  void writeSize(int value) {
	fseek(f,0,SEEK_SET);
	fwrite(&value,sizeof(int),1,f);
  }
 public:
  typedef T value_type;

  // only const iterator because derefernce method does not allow modifying data
  class const_iterator {
	FILE* file; // set this using what is passed in from the constructor
	int index; // the current index in the array
	void iterate() {index++;}
	void diterate() {index--;}
  public:
	const_iterator(int i,FILE *f) {  // pass in FileArrayList's f pointer
	  file=f;
	  index=i;
	}
	const_iterator(const const_iterator &i) {
	  file=i.file;
	  index=i.index;
	}
	T operator*() {
	  return read(index,file);
	}
	bool operator==(const const_iterator &i) const {
	  return index == i.index && file == i.file;
	}
	bool operator!=(const const_iterator &i) const {
	  return index != i.index || file != i.file;
	}
	const_iterator &operator=(const const_iterator &i) {
	  file = i.file;
	  index = i.index;
	  return *this;
	}
	const_iterator &operator++() {
	  iterate();
	  return *this;
	}
	const_iterator &operator--() {
	  diterate();
	  return *this;
	}
	const_iterator operator++(int) {
	  auto cIter = *this;
	  iterate();
	  return cIter;
	}
	const_iterator operator--(int) {
	  auto cIter = *this;
	  diterate();
	  return cIter;
	}
	

	friend class FileArrayList;
  };

  // General Methods
  FileArrayList(const std::string &fname) { // constructor, takes a filename argument
	f = fopen(fname.c_str(), "r+");
	if (f==nullptr) {
	  // file does not exist, so reopen the file with w+
	  f = fopen(fname.c_str(),"w+");
	  writeSize(0); // write the starting size of zero to the file
	} // otherwise everything is stored in the file, so no additional setup required
  }

  template<typename I>  // The type I is an iterator of any type.
	FileArrayList(I begin,I end,const std::string &fname) { // copy constructor
	f = fopen(fname.c_str(),"w+");
	writeSize(0);
	for (auto i=begin; i!=end; i++) {
	  push_back(*i);  // derefence i to get its data
	}
  }
  ~FileArrayList() {
	fclose(f);
  }
  void push_back(const T &t) {
	write(t,size());
	writeSize(size()+1);
  }
  void pop_back() {
	writeSize(size()-1);
  }
  int size() const {
	fseek(f,0,SEEK_SET);
	int sz;
	fread(&sz,sizeof(int),1,f);
	return sz;
  }
  void clear() {
	writeSize(0);
  }
  const_iterator insert(const_iterator position, const T &t) {
	writeSize(size()+1);
	for(auto i = end(); i!=position; i--) {
	  T tmp = read(i.index-1,f);
	  write(tmp,i.index);
	}
	write(t,position.index);
	return position;
  }
  T operator[](int index) const {
	return read(index,f);
  }
  const_iterator erase(const_iterator position) {
	writeSize(size()-1);
	if (position != end()) { // you would not want to run this on the end, nothing needs to shift
	  for(auto i = position; i!=end(); i++) {
		T tmp = read(i.index+1,f);
		write(tmp,i.index);
	  }
	}
	return position;
  }
  void set(const T &value,int index) {
	write(value,index);
  }
  const_iterator begin() {
	return const_iterator(0,f);
  }
  const_iterator begin() const {
	return const_iterator(0,f);
  }
  const_iterator end() {
	return const_iterator(size(),f);
  }
  const_iterator end() const {
	return const_iterator(size(),f);
  }
  const_iterator cbegin() const {
	return const_iterator(0,f);
  }
  const_iterator cend() const {
	return const_iterator(size(),f);
  }
};
